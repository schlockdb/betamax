import mongoose from "mongoose";
import { MONGO_LOCAL, MONGO_PROD } from "./const/db";
mongoose.Promise = global.Promise;

const url = process.env.NODE_ENV === "production" ? MONGO_PROD : MONGO_LOCAL;

mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
mongoose.connection.once("open", () =>
  console.log(`Connected to mongo at ${url}`)
);
