import { ApolloServer, gql } from "apollo-server-express";
import express from "express";
import session from "express-session";
import passport from "passport";
import { GraphQLLocalStrategy, buildContext } from "graphql-passport";
import { v4 as uuid } from "uuid";
import { schema } from "./schema";
import UserModel from "./models/user";
import bcrypt from "bcrypt";

import "./config";
import { User } from "./objects/user"; // MongoDB connection

passport.use(
  new GraphQLLocalStrategy((email, password, done) => {
    UserModel.findOne({ email }, (err, user) => {
      if (err) {
        console.log("LOGIN ERROR", err);
        done(err, null);
      }

      if (bcrypt.compareSync(password, user.password)) {
        done(null, user);
      } else {
        done(new Error("Incorrect Username or Password"), null);
      }
    });
  })
);

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(async function (id, done) {
  const user = new User(await UserModel.findOne({ _id: id }));
  done(null, user);
});

const app = express();
app.use(
  session({
    genid: (req) => uuid(),
    secret: process.env.SECRET || "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

const server = new ApolloServer({
  schema,
  context: ({ req, res }) => buildContext({ req, res, UserModel }),
  playground: {
    settings: {
      "request.credentials": "same-origin",
    },
  },
});

server.applyMiddleware({ app });

const env = process.env.NODE_ENV || "development";

const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send("SchlockDB Server Running");
});

app.listen(port, () =>
  console.log(
    `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
  )
);
