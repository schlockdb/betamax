import mongoose from "mongoose";
import { movies as schlockData } from "../schlockMovies";
import Schlock from "../../models/schlock";
import _ from "lodash";
import { MONGO_LOCAL, MONGO_PROD } from "../../const/db";

async function schlockMovieLoader() {
  console.log("***Loading Schlock Movies to MongoDB***");
  await mongoose.connect(MONGO_PROD, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  //Add all the movies to DB
  for (let item in schlockData) {
    await Schlock.findOne(
      { name: schlockData[item].title },
      async (err, state) => {
        console.log(
          "Processing the following schlock: ",
          schlockData[item].title
        );
        if (err) {
          console.log("ERROR: " + err);
        } else {
          if (_.isNil(state)) {
            console.log(
              "Following schlock not found: ",
              schlockData[item].title
            );
            await Schlock.create(
              {
                title: schlockData[item].title,
                genre: schlockData[item].genre,
                director: schlockData[item].director,
                runtime: schlockData[item].runtime,
                budget: schlockData[item].budget,
                cover: schlockData[item].cover,
                quote: schlockData[item].quote,
                description: schlockData[item].description,
                trailer: schlockData[item].trailer,
                take: schlockData[item].take,
                stream: schlockData[item].stream,
              },
              function (err, movie) {
                if (err) {
                  console.log("ERROR: " + err);
                } else {
                  console.log("Saved! : ", movie);
                }
              }
            );
          } else {
            console.log(
              "Following schlock was FOUND: ",
              schlockData[item].title
            );
          }
        }
      }
    );
  }
  await mongoose.disconnect();
}

schlockMovieLoader();
