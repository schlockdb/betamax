export const movies = [
    {
        title: 'Miami Connection',
        genre: ['Martial Arts', 'Action', 'Thriller'],
        director: [
            'Richard Park',
            'Y.K. Kim'
        ],
        runtime: 84,
        budget: "$1 Million",
        cover: "https://upload.wikimedia.org/wikipedia/en/7/79/Miami_Connection_movie_poster.jpg",
        quote: "My father I found my father....Oh my Gawd!!",
        description: "A group of motorcycle ninjas rules over the Miami drug scene has taken over the club where ninja band " +
            "Dragon Sound plays. It is now up to Dragon Sound to take back their club by using their Taekwondo and music skills.",
        take: "Taekwondo Rock band vs Ninja Drug Lords in Miami? Sign me up!",
        trailer: "CBuQ-WaWmjI",
        year: "1987",
        stream: [
            {
                service: "IMDb TV",
                url: "https://www.imdb.com/tv/watch/tt0092549",
                paid: false,
                sub: false
            },
            {
                service: "tubi",
                url: "https://tubitv.com/movies/410024/miami_connection",
                paid: false,
                sub: false
            },
            {
                service: "VUDU",
                url: "https://www.vudu.com/content/movies/details/Miami-Connection/394824",
                paid: false,
                sub: false
            },
            {
                service: "iTunes",
                url: "https://itunes.apple.com/us/movie/miami-connection/id865758980",
                paid: true,
                sub: false
            },
            {
                service: "Google Play",
                url: "https://play.google.com/store/movies/details/Miami_Connection?gl=US&hl=en&id=yfAK_ebqHRM",
                paid: true,
                sub: false
            }
        ]
    },
    {
        title: 'Samurai Cop',
        genre: ['Action', 'Thriller'],
        director: [
            'Amir Shervan'
        ],
        runtime: 96,
        budget: "Unknown",
        cover: "https://m.media-amazon.com/images/M/MV5BZjI2N2Q3NDUtZTI3MS00ZmVmLWFlN2ItNzBmODQ3ZDExNjZkXkEyXkFqcGdeQXVyMjM2OTAxNg@@._V1_UY1200_CR135,0,630,1200_AL_.jpg",
        quote: "What does katana mean? It means Japanese Sword",
        description: "A bitter samurai cop and his sidekick intend to get their revenge and bravely try to go after the Yakuza.",
        take: "This is possibly one of the best movies you have never seen.",
        trailer: "l6QRGQ9Rt7w",
        year: "1989",
        stream: [
            {
                service: "Prime Video",
                url: "https://www.amazon.com/Samurai-Cop-Matt-Hannon/dp/B01G4DXMMO",
                paid: false,
                sub: true
            },
            {
                service: "tubi",
                url: "https://tubitv.com/movies/324874/samurai_cop",
                paid: false,
                sub: false
            }
        ]
    },
    {
        title: 'Roar',
        genre: ['Comedy Horror'],
        director: [
            'Noel Marshall'
        ],
        runtime: 98,
        budget: "$17 Million",
        cover: "https://m.media-amazon.com/images/M/MV5BMTQ1ODM3NDY3Nl5BMl5BanBnXkFtZTgwNzY1NzQ3NDE@._V1_UY1200_CR91,0,630,1200_AL_.jpg",
        quote: "It's just playing, I'm tellin' ya! ***Tiger proceeds to attack actor***",
        description: "A woman and her family travel to Africa to meet a scientist who lives with wild animals.",
        take: "Hmm...recording a movie about how great Tigers and Lions are in a ranch with 150 UNTRAINED Tigers and Lions. Who thought this was a great idea?",
        trailer: "0b7msPs6EJY",
        year: "1981",
        stream: [
            {
                service: "YouTube",
                url: "https://www.youtube.com/watch?v=qj3KKPI7Yyk",
                paid: false,
                sub: false
            }
        ]
    },
    {
        title: 'The Room',
        genre: ['Drama?'],
        director: [
            'Tommy Wiseau'
        ],
        runtime: 99,
        budget: "$6 Million",
        cover: "https://thefridacinema.org/wp-content/uploads/2019/06/the-room-poster-1.jpg",
        quote: "You are lying! I never hit you! YOU ARE TEARING ME APART, LISA!",
        description: "Johnny is a successful banker who lives happily in a San Francisco townhouse with his fiancée, Lisa. One day, inexplicably, she gets bored of him and decides to seduce Johnny's best friend, Mark. From there, nothing will be the same again.",
        take: "Cult classic that many people consider a pioneer on the So Bad is Good genre.",
        trailer: "zVR6QrLwi8I",
        year: "2003",
        stream: []
    },
    {
        title: 'The Last Vampire On Earth',
        genre: ['Romance?'],
        director: [
            'Vitaliy Versace'
        ],
        runtime: 90,
        budget: "$250,000",
        cover: "https://dvdlady.com/image/2019/08/the-last-vampire-on-earth-2010-starring-rachel-terzak-on-dvd-1.jpg",
        quote: "Stop or I'll shoot. This is a good man and if you took the time to get to know him you would find that out. He doesn't want to hurt us. He doesn't feed off of humans. He wants to help us. He wants to help find cures for diseases. He wants to help find cures for diseases like AIDs. Maybe that doesn't mean much to you but it means a lot to people like me who have it. That's right I have AIDs and it's advancing very quickly.",
        description: "Chloe (McKenzie Grimmet) reaches out to a shy and pale student named Aurelius (Michael Bole),and surprising things happen.",
        take: "It has finally happened. A worst story than Twilight! ",
        trailer: "5ABGst_JNYU",
        year: "2009",
        stream: [
            {
                service: "tubi",
                url: "https://tubitv.com/movies/460875",
                paid: false,
                sub: false
            }
        ]

    },
    {
        title: 'Spookies',
        genre: ['Horror'],
        director: [
            'Brendan Faulkner',
            'Thomas Doran',
            'Eugenie Joseph'
        ],
        runtime: 85,
        budget: "Unknown",
        cover: "https://m.media-amazon.com/images/M/MV5BYTcxYmFkOTItM2QwYy00OGM0LWJhOWEtYTAwNTkxYmQ1YzlmXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg",
        quote: "My victims, walk in the land of the dead. They are all my children, and I am their ultimate lord...",
        description: "A dilapidated mansion becomes a slaughterhouse when an evil sorcerer unleashes his monstrous sidekicks on intruders.",
        take: "What happens when you have amazing practical effects, limited budget and a movie that ran out of a plot in the middle of it? This movie!",
        trailer: "WAC-Ofmu9T4",
        year: "1986",
        stream: [
            {
                service: "YouTube",
                url: "https://www.youtube.com/watch?v=GO2IXMpbxL0",
                paid: false,
                sub: false
            }
        ]
    },
    {
        title: 'Ice Cream Man',
        genre: ['Horror'],
        director: [
            'Paul Norman'
        ],
        runtime: 83,
        budget: "$2 Million",
        cover: "https://m.media-amazon.com/images/M/MV5BNzAxN2Q0MTUtNWI4Yi00YjRiLWI2NjAtMTg3NTdiYjQ2NGVkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg",
        quote: "I guess not every day is a happy, happy, happy day!",
        description: "Poor Gregory. After being released from the Wishing Well Sanatorium, all he wants to do is make the children happy. So Gregory reopens the old ice cream factory, and all the unappreciative brats are reprocessed into the flavor of the day.",
        take: "In this movie you can see an Ice Cream Man jump from his truck and while in the air scoop two people, pretty amazing!",
        trailer: "jXN27sMCbeM",
        year: "1995",
        stream: [
            {
                service: "YouTube",
                url: "https://www.youtube.com/watch?v=mQjM8kcTvHg",
                paid: false,
                sub: false
            },
            {
                service: "Prime Video",
                url: "https://www.amazon.com/Ice-Cream-Man-Clint-Howard/dp/B08C35DRRM",
                paid: false,
                sub: true
            }
        ]
    },
    {
        title: 'Space Raiders',
        genre: ['Sci-fi', "Action"],
        director: [
            'Howard R. Cohen'
        ],
        runtime: 84,
        budget: "Unknown",
        cover: "https://images-na.ssl-images-amazon.com/images/I/913zfBSCIUL._SL1500_.jpg",
        quote: "These aren't real onions, are they? They're some kind of alien yucko onions. Wonder if this is real cheese?",
        description: "A futuristic, sensitive tale of adventure and confrontation when a 10 year old boy is accidentally kidnapped by a spaceship filled with a motley crew of space pirates.",
        take: "It's a B version of Star Wars!",
        trailer: "XBham6MnKYE",
        year: "1983",
        stream: [
            {
                service: "Shout Factory TV",
                url: "https://www.shoutfactorytv.com/space-raiders/5c198aa373c3f912c8002daa",
                paid: false,
                sub: false
            }
        ]
    },
];