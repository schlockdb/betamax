export function User(rawUser) {
  this.id = rawUser._id;
  this.firstName = rawUser.firstName;
  this.lastName = rawUser.lastName;
  this.email = rawUser.email;
}
