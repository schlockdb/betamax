export const typeDef = `
  type Movie {
    id: ID!
    title: String
    genre: [String]
    director: [String]
    runtime: Int
    budget: String
    cover: String
    quote: String
    description: String
    trailer: String
    take: String
    stream: [Stream]
  }
`;
