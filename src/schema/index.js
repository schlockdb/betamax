import { typeDef as MovieTypeDef } from "./movie.js";
import { typeDef as UserTypeDef } from "./user.js";
import { typeDef as StreamTypeDef } from "./stream.js";
import { makeExecutableSchema } from "graphql-tools";
import Schlock from "../models/schlock";
import UserModel from "../models/user";
import { User } from "../objects/user";
import _ from "lodash";
import { error } from "../util";

const Query = `
  type Query {
    allMovies: [Movie!]!
    getMovie(id: ID!): Movie
    currentUser: User
  }
`;

const Mutation = `
type Mutation {
    addSchlock(schlock: addSchlockInput!): Movie
    signUp(user: signUpInput): User
    login(auth: loginInput): User
    logout: Boolean
  }
  
  input signUpInput {
      firstName: String!
      lastName: String!
      email: String!
      password: String!
  }
  
  input addSchlockInput {
      firstName: String!
      lastName: String!
      email: String!
      password: String!
  }
  
  input loginInput {
      email: String!
      password: String!
  }
`;

const resolvers = {
  Query: {
    allMovies: async () => await Schlock.find({}).exec(),
    getMovie: async (parent, args) => await Schlock.findById(args.id),
    currentUser: (parent, args, context) => context.getUser(),
  },
  Mutation: {
    addSchlock: async (_, args) => {
      try {
        let response = await Schlock.create(args);
        return response;
      } catch (e) {
        return e.message;
      }
    },
    login: async (parent, args, context) => {
      const { email, password } = args.auth;

      if (_.isNil(email) || _.isNil(password)) {
        error("Invalid payload");
      }
      const { user, info } = await context.authenticate("graphql-local", {
        email,
        password,
      });

      const currentUser = new User(user);
      await context.login(currentUser);
      return currentUser;
    },
    logout: (parent, args, context) => context.logout(),
    signUp: async (parent, args, context) => {
      const { firstName, lastName, email, password } = args.user;

      if (
        _.isNil(email) ||
        _.isNil(password) ||
        _.isNil(firstName) ||
        _.isNil(lastName)
      ) {
        error("Invalid payload");
      }

      const existingUsers = await UserModel.find({ email });

      if (existingUsers.length !== 0) {
        error("User with email already exists");
      }

      const newUser = await UserModel.create({
        firstName,
        lastName,
        email,
        password,
      });
      return new User(newUser);
    },
  },
};

export const schema = makeExecutableSchema({
  typeDefs: [Query, Mutation, MovieTypeDef, UserTypeDef, StreamTypeDef],
  resolvers,
});
