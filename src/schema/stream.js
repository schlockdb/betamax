export const typeDef = `
 type Stream {
    service: String
    url: String
    paid: Boolean
    sub: Boolean
  }`;
