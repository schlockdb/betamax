export const error = (message) => {
  return throw new Error(message);
};
