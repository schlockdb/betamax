import mongoose from 'mongoose';

let SchlockSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    genre: {
        type: [String],
        unique: false,
        required: true
    },
    director: {
        type: [String],
        unique: false,
        required: true
    },
    runtime: {
        type: Number,
        unique: false,
        required: true
    },
    budget: {
        type: String,
        unique: false,
        required: true
    },
    cover: {
        type: String,
        unique: false,
        required: true
    },
    quote: {
        type: String,
        unique: false,
        required: true
    },
    description: {
        type: String,
        unique: false,
        required: true
    },
    trailer: {
        type: String,
        unique: false,
        required: false
    },
    take: {
        type: String,
        unique: false,
        required: true
    },
    stream: [{
        service: String,
        url: String,
        paid: Boolean,
        sub: Boolean
    }]
});

let Schlock = mongoose.model('Schlock', SchlockSchema);
module.exports = Schlock;
