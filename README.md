```

  ____  ______ _______       __  __          __   __
 |  _ \|  ____|__   __|/\   |  \/  |   /\    \ \ / /
 | |_) | |__     | |  /  \  | \  / |  /  \    \ V / 
 |  _ <|  __|    | | / /\ \ | |\/| | / /\ \    > <  
 | |_) | |____   | |/ ____ \| |  | |/ ____ \  / . \ 
 |____/|______|  |_/_/    \_\_|  |_/_/    \_\/_/ \_\
                                                    
```

# SchlockDB Backend

## Project Description

This is an experiment project that I made just to play with the MERN (GraphQL, Express, React, Node) Stack. 

In this project you can interact with a collection of Schlock Movies.

Betamax is the GraphQL backend for the frontend which is [VHS](https://gitlab.com/schlockdb/vhs).

## How to run

In the project directory, you can run:

```
npm install
npm run dev
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
To access the GraphQL playground simply go to [http://localhost:3006/graphql](http://localhost:3006/graphql). 

## Design

### Application Overview

SchlockDB consists of two parts:

- VHS - React/Redux Frontend
- Betamax - GraphQL Backend

The Betamax docker image is build and pushed to DockerHub from the GitLab CI/CD. This docker hub image is then 
deployed into the KubeSail Kubernetes instance. This Kubernetes instance has limited resources, and the uptime is
decent. To check that the app is running, we have a postman monitor checking the performance and live-ness every hour.

As for VHS, the deployment is a little more simple. Gitlab CI/CD still handles Unit testing and functional testing.
But the difference here is that we have the CDN provider pull the code and deploy it across their CDN network. 
This provides us with rock solid performance and uptime for the front end. 


![Overview](./docs/SchlockDB.png)

### Postman GraphQL Documentation
[Postman Documentation](https://documenter.getpostman.com/view/4118863/T1LQi6u3?version=latest)

[Server Performance Monitor](https://web.postman.co/monitors/1eae1090-d768-4d90-a232-7e0c28e21095)

## TODO List
This is check list of items Derek needs to work on
- Migrate data to MongoDB
- Add user management support
- Filtering and search queries
- Add a more robust kubernetes instance
- Clean the docker pipeline
- Movie preferences support
- Add a "Where to stream" system

Research

- Create YouTube channel to host all the trailers from one location.
- Check if there is a way to allow streaming these hard to find titles from SchlockDB
- Check out "Video Vortex" 

### [SCHLOCKDB.COM](http://www.schlockdb.com)