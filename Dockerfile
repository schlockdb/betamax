FROM node:12

WORKDIR /app
ENV PORT=3000

COPY . .

RUN npm install

EXPOSE $PORT

CMD ["npm", "start"]